from flask import Flask
import json
import sqlite3
import pandas as pd
import matplotlib.pyplot as plt
import base64
from io import BytesIO
from datetime import datetime, timedelta


app = Flask(__name__)

@app.route('/bsstats/<fromdate>')
def listing_handler(fromdate='20200315010000'):
    conn = sqlite3.connect('stats.db')
    #fromdate = (datetime.now()-timedelta(2)).strftime('%Y%m%d%H%M%S')
    rows = conn.execute('select dt, playerid, playername, bsscore from bsstats where '+\
                  'dt > ''{}'' order by dt desc'.format(fromdate)).fetchall()

    js = [{'dt':r[0], 'id':r[1], 'name':r[2], 'score':r[3]} for r in rows]
    return json.dumps(js)


@app.route('/bsstats2/<int:fp>/<int:lp>')
def listing_handler2(fp=1, lp=10):
    conn = sqlite3.connect('stats.db')
    fromdate = (datetime.now() - timedelta(2)).strftime('%Y%m%d%H%M%S')
    df = pd.read_sql_query('select dt, playername, bsscore from bsstats where ' +
                           'dt > ''{}'' order by dt desc'.format(fromdate), conn)
    conn.close()

    fig, axes = plt.subplots(3)

    df['place'] = df.sort_values(['dt', 'bsscore'], ascending=[True, False]).groupby('dt').cumcount() + 1
    dfp = df.loc[(df['place'] >= fp) & (df['place'] <= lp)].sort_values(['dt', 'bsscore'], ascending=[True, False])

    axes[0].invert_yaxis()
    for ax in axes:
        ax.tick_params(labelsize=4)

    for name, group in dfp.groupby('playername'):
        group.plot(x='dt', y='place', ax=axes[0], label=name)
    for name, group in dfp.groupby('playername'):
        group.plot(x='dt', y='bsscore', ax=axes[1], label=name)

    handles, labels = axes[0].get_legend_handles_labels()
    labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    axes[0].legend(handles, labels, ncol=10, fontsize=4, loc='lower left',
                   bbox_to_anchor=(0, 1, 1, 0.4))

    dfp['dif'] = dfp.groupby('playername')['bsscore'].diff()
    dfp['ma24'] = dfp.groupby('playername')['dif'].rolling(24).mean().reset_index(0, drop=True)

    for name, group in dfp.groupby('playername'):
        group.plot(x='dt', y='ma24', ax=axes[2], label=name)
    del df
    del dfp

    #handles, labels = axes[0].get_legend_handles_labels()
    #labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    axes[0].legend(handles, labels, ncol=10, fontsize=4, loc='lower left',
                   bbox_to_anchor=(0, 1, 1, 0.4))
    axes[1].legend(loc='lower left', fontsize=5, ncol=4)
    axes[2].legend(loc='lower left', fontsize=5, ncol=4)

    buf = BytesIO()

    fig.savefig(buf, format='png', dpi=300)
    data = base64.b64encode(buf.getbuffer()).decode('ascii')
    return '<img src="data:image/png;base64,{}" width="1200" height="1200" alt="graph"/>'.format(data)


if __name__ == '__main__':
    app.run(host = '37.59.154.243', port = 80)